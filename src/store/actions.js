import * as actionTypes from './actionTypes';
import axios from '../axios-date';

export const setNumOfDays = (dayOfLoss) => {
  return {
    type: actionTypes.SET_NUM_OF_DAYS,
    dayOfLoss: dayOfLoss
  }
}

export const countDays = () => {
  return dispatch => {
    axios.get('https://cardtimer-database.firebaseio.com/last-day-of-loss.json').then(response => {
      dispatch(setNumOfDays(response.data))
    })
      .catch(error => {
        console.log('error.message');
        console.log(error.message);
      });
  };

};
