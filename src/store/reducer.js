import * as actionTypes from './actionTypes';

import { updateObject } from './utility';

const initialState = {
	counter: 0
}

const setNumOfDays = (state, action) => {
	const lastLossDay = new Date(action.dayOfLoss).toDateString();
	const currentDate = new Date().toDateString();
	const distinction = Math.abs(Math.floor((Date.parse(lastLossDay) - Date.parse(currentDate)) / 86400000));
	return updateObject(state, { counter: distinction })
}

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionTypes.SET_NUM_OF_DAYS: return setNumOfDays(state, action)
		default:
			return state
	}
}

export default reducer;