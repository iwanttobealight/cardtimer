import React from 'react';
import PropTypes from 'prop-types';

import './CardTimer.scss';

const CardTimer = (props) => (
  <React.Fragment>
    <h2 className="title">Дней без потери карты от столовки:</h2>
    <div className="counter">{props.counter}</div>
  </React.Fragment>
)

CardTimer.propTypes = {
  counter: PropTypes.number.isRequired
}

export default CardTimer;
