import React from 'react';
import { render } from 'react-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

// если я импортирую App линтер ругается втф линтер
import MyApp from './containers/App/App';
import reducer from './store/reducer';

import './styles.scss';

const composeEnhancers = window.__REDUX__DEVTOOLS__EXTENSION__COMPOSE || compose;

const store = createStore(reducer, composeEnhancers(applyMiddleware(thunk)));

render(<Provider store={store}><MyApp /></Provider>, document.getElementById('app'));
