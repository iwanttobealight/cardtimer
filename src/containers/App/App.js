import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as actionCreators from '../../store/index';

import CardTimer from '../../components/CardTimer/CardTimer';

import './App.scss'

export class App extends Component {
  componentDidMount() {
    this.props.increment();
  };

  render() {
    return (
      <div className="app">
        <CardTimer counter={this.props.counter} />
      </div>
    )
  }
}

App.propTypes = {
  counter: PropTypes.number.isRequired,
  increment: PropTypes.func.isRequired
}

const mapStateToProps = state => {
  return { counter: state.counter }
}

const mapDispatchToProps = dispatch => {
  return {
    increment: () => dispatch(actionCreators.countDays())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);